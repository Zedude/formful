import React, { useEffect, useState } from 'react'
import { useHistory, useLocation } from 'react-router'
import { Answer, Button, Checkbox, Input } from '../components';
import { useFirebase } from '../firebase';
import _ from 'lodash'
import shortid from 'shortid'
import { useAuth } from '../provider';

export const FillForm = () => {
  const location = useLocation()
  const { firestore } = useFirebase();
  const path = location.pathname.slice(1)
  const [questions, setQuestions] = useState([])
  const [answers, setAnswers] = useState([])
  const history = useHistory()
  const [title, setTitle] = useState('')
  const { user } = useAuth()

  useEffect(() => {
    if (firestore) {
      firestore.doc(`forms/${path}`).get()
        .then((d) => {
          if (d.exists) {
            setQuestions(d.data().questions)
            setTitle(d.data().title)
          }
        })
    }
  }, [firestore, location])

  const submit = async () => {
    if (user.uid) {
      if (_.compact(answers).length === questions.length && path) {
        const id = await shortid.generate()
        console.log(answers)
        await firestore.doc(`forms/${path}/form/${id}`).set({
          answers: answers,
          user: user.email,
        })
        history.push('/')
      } else {
        alert('fill in all questions')
      }
    } else {
      alert('You must login to fill form')
    }
  }

  const changeAnswers = (e, i) => {
    let answer = answers
    answer[i] = {
      index: i,
      answer: e.options ? e[i] : e,
      question: questions[i],
    }
    setAnswers(answer);
  }

  return (
    <div>
      {title && title}
      {questions &&
        questions.map((el, i) => {
          return (
            <>
              <Answer
                question={el.question}
                key={i}
                options={el.options}
              >
                {el.type === 'answer' &&
                  <Input value={answers[i]} onChange={(e) => { changeAnswers(e.target.value, i) }} placeholder='Your Answer' width='360px' />
                }
                {el.type === 'checkbox' &&
                  <Checkbox setChoice={(el) => { changeAnswers(el, i) }} options={el.options}></Checkbox>
                }
                {el.type === 'choice' &&
                  <Checkbox setChoice={(el) => { changeAnswers(el, i) }} multiple options={el.options} />
                }
              </Answer>
            </>
          )
        })}
      <Button onClick={() => { submit() }}>Submit</Button>
    </div>
  )
}