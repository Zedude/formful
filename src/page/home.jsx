import React from 'react'
import { useHistory } from 'react-router'
import { Button } from '../components'
import { useAuth } from '../provider'

export const Home = () => {
  const { user } = useAuth()
  const history = useHistory()

  return (
    <div>
      <Button onClick={() => { history.push('/make-form') }}>Make a new form</Button>
      {user &&
        <Button onClick={() => { history.push('/my-forms') }}>Your forms</Button>
      }
      {!user &&
        <>
          <Button onClick={() => { history.push('/signin') }}>Sign In</Button>
          <Button onClick={() => { history.push('/signup') }}>Sign Up</Button>
        </>
      }
    </div>
  )
}