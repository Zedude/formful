import _ from 'lodash'
import React, { useState } from 'react'
import { Button, Input, Answer, Checkbox } from '../components'
import { useFirebase } from '../firebase'
import shortid from 'shortid'
import { useHistory } from 'react-router'
import { useAuth } from '../provider'

export const MakeForm = () => {
  const [questions, setQuestions] = useState([])
  const [add, setAdd] = useState({})
  const [type, setType] = useState('')
  const [question, setQuestion] = useState('')
  const [options, setOptions] = useState('')
  const [title, setTitle] = useState('')
  const { firestore } = useFirebase()
  const { user } = useAuth()
  const history = useHistory()
  const [loginReq, setLoginReq] = useState(false)

  const uploadForm = async () => {
    if (user && user.uid) {
      if (title) {
        const id = shortid.generate()
        await firestore.doc(`forms/${id}`).set({
          questions: _.drop(questions),
          id: id,
          title: title,
          author: user.uid
        })
        await firestore.doc(`users/${user.uid}/forms/${id}`).set({
          questions: _.drop(questions),
          id: id,
          title: title
        })
        history.push('/')
      } else {
        alert('Add a title')
      }
    } else {
      alert('You must login to make a form')
    }
  }

  const addQuestion = () => {
    if (type && question) {
      const option = _.split(options, ',')
      setAdd({
        type: type,
        question: question,
        options: option ? option : undefined
      })
      setQuestions([...questions, add])
      setQuestion('')
      setOptions('')
    }
  }

  const Render = () => {
    return questions.map((e, i) => {
      return (
        <>
          <Answer
            key={i}
            question={e.question}
          >
            {e.type === 'answer' &&
              <Input placeholder='Your Answer' width='360px' />
            }
            {e.type === 'checkbox' &&
              <Checkbox options={e.options}></Checkbox>
            }
            {e.type === 'choice' &&
              <Checkbox multiple options={e.options}></Checkbox>
            }
          </Answer>
        </>
      )
    })
  }

  return (
    <div>
      <Render />
      <div>
        <Button type={type === 'answer' ? 'primary' : 'secondary'} onClick={() => { setType('answer') }}>Answer</Button>
        <Button type={type === 'checkbox' ? 'primary' : 'secondary'} onClick={() => { setType('checkbox') }}>Checkbox</Button>
        <Button type={type === 'choice' ? 'primary' : 'secondary'} onClick={() => { setType('choice') }}>Multiple choice</Button>
        <Input width={500} onChange={(e) => { setQuestion(e.target.value) }} value={question} placeholder='Question'></Input>
        {type !== 'answer' &&
          <Input width={500} value={options} onChange={(e) => { setOptions(e.target.value) }} placeholder='seperate options with a coma' />
        }
        <Button onClick={() => { addQuestion() }}>Add Question</Button>
        <Button onClick={() => { uploadForm() }}>Make Form</Button>
        <Input onChange={(e) => {setTitle(e.target.value)}} value={title} width={500} placeholder='Title' />
        <p>Require login</p>
        <Button onClick={() => {setLoginReq(!loginReq)}}>{loginReq ? 'Yes' : 'No'}</Button>
      </div>
    </div>
  )
}