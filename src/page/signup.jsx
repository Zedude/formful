import React, { useState } from 'react'
import { useHistory } from 'react-router'
import { Button, Input } from '../components'
import { useAuth } from '../provider'

export const SignUp = () => {
  const [email, setEmail] = useState('')
  const [pass, setPass] = useState('')
  const [passConf, setPassConf] = useState('')
  const [error, setError] = useState('')
  const { signup } = useAuth()
  const history = useHistory()

  const signUp = async () => {
    if (!email || !pass || !passConf) {
      setError('Enter the required fields')
      return
    }
    if (pass !== passConf) {
      setError('Passwords do not match')
      return
    }

    try { 
      await signup(email, pass) 
      history.push('/')
    } catch (e) {
      setError(e.message)
    }
  }

  return (
    <div>
      <Input value={email} onChange={(e) => {setEmail(e.target.value)}} placeholder='email' />
      <Input type='password' value={pass} onChange={(e) => {setPass(e.target.value)}} placeholder='password' />
      <Input type='password' value={passConf} onChange={(e) => {setPassConf(e.target.value)}} placeholder='password' />
      <Button onClick={signUp}>Sign Up</Button>
      {error}
    </div>
  )
}