import React, { useEffect, useState } from 'react'
import { useHistory, useLocation } from 'react-router'
import { Answer, Button, Checkbox, Input, Response } from '../components'
import { useFirebase } from '../firebase'
import { useAuth } from '../provider'

export const AuthorForm = () => {
  const [tab, setTab] = useState('form')
  const { firestore } = useFirebase()
  const [questions, setQuestions] = useState([])
  const [title, setTitle] = useState('')
  const location = useLocation()
  const [responses, setResponses] = useState([])
  const path = location.pathname.split('/')[2]
  const history = useHistory()
  const { user } = useAuth()

  useEffect(() => {
    if (firestore && user) {
      firestore.doc(`forms/${path}`).get()
        .then((d) => {
          if (d.exists) {
            setQuestions(d.data().questions)
            setTitle(d.data().title)
            if (d.data().author !== user.uid) {
              history.push('/')
            }
          }
        })
      firestore.collection(`forms/${path}/form`).onSnapshot((q) => {
        setResponses(q.docs.map((x) => x.data()));
        console.log(q.docs.map((x) => x.data()))
      })
    }
  }, [firestore, location, user])

  return (
    <div>
      <div className='row'>
        <Button onClick={() => { setTab('form') }} type={tab === 'form' ? 'primary' : 'secondary'}>Form</Button>
        <Button onClick={() => { setTab('response') }} type={tab === 'response' ? 'primary' : 'secondary'}>Reponses</Button>
      </div>
      {tab === 'form' &&
        <div>
          {title && title}
          {questions &&
            questions.map((e, i) => {
              return (
                <>
                  <Answer
                    question={e.question}
                    key={i}
                    options={e.options}
                  >
                    {e.type === 'answer' &&
                      <Input placeholder='Your Answer' width='360px' />
                    }
                    {e.type === 'checkbox' &&
                      <Checkbox options={e.options}></Checkbox>
                    }
                    {e.type === 'choice' &&
                      <Checkbox multiple options={e.options} />
                    }
                  </Answer>
                </>
              )
            })}
        </div>
      }
      {tab === 'response' &&
        <div>
          {/* Going through everyone's responses */}
          {responses &&
            responses.map((element) => {
              // Going through the response of one person
              return (
                <div className='responseContainer'>
                  {element.user}
                  {element.answers.map((e, i) => {
                    return (
                      <>
                        <Response key={i} question={e.question.question} response={e.answer} >

                        </Response>
                      </>
                    )
                  })}
                </div>
              )
            })
          }
          {!responses && 
            <div>No responses yet</div>
          }
        </div>
      }
    </div>
  )
}