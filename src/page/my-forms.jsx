import React, { useEffect, useState } from 'react'
import { useHistory } from 'react-router'
import { Button } from '../components'
import { useFirebase } from '../firebase'
import { useAuth } from '../provider'

export const MyForms = () => {
  const { firestore } = useFirebase()
  const { user } = useAuth()
  const [forms, setForms] = useState([])
  const history = useHistory()

  useEffect(() => {
    console.log(user)
    if (firestore && user) {
      firestore.collection(`users/${user.uid}/forms`).onSnapshot((q) => {
        setForms(q.docs.map((x) => x.data()));
      })
    }
  }, [firestore, user])

  return (
    <div>
      {forms && forms.map((e, i) => {
        return (
          <>
            <p>
              Id: {e.id}
            </p>
            <Button onClick={() => { history.push(`/author-form/${e.id}`) }} key={i}>
              <div className='row'>
                <p>
                  Title: {e.title}
                </p>
              </div>
            </Button>
          </>
        )
      })}
      {!forms &&
        <div>You haven't made any forms yet :(</div>
      }
    </div>
  )
}