import React, { useState } from 'react'
import { useHistory } from 'react-router'
import { Button, Input } from '../components'
import { useAuth } from '../provider'

export const SignIn = () => {
  const [email, setEmail] = useState('')
  const [pass, setPass] = useState('')
  const [error, setError] = useState('')
  const { signin } = useAuth()
  const history = useHistory()

  const signIn = async () => {
    if (!email || !pass) {
      setError('Enter the required fields')
      return
    }

    try { 
      await signin(email, pass) 
      history.push('/')
    } catch (e) {
      setError(e.message)
    }
  }

  return (
    <div>
      <Input value={email} onChange={(e) => {setEmail(e.target.value)}} placeholder='email' />
      <Input type='password' value={pass} onChange={(e) => {setPass(e.target.value)}} placeholder='password' />
      <Button onClick={signIn}>Sign In</Button>
      {error}
    </div>
  )
}