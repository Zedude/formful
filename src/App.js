import React from "react";
import {
  BrowserRouter as Router,
  Route,
  Switch
} from "react-router-dom";
import { FillForm, Home, MakeForm, SignIn, SignUp, MyForms, AuthorForm } from "./page/";
import { AuthProvider } from "./provider";

const App = () => {
  return (
    <AuthProvider>
      <Router>
        <div>
          <Switch>
            <Route path="/make-form">
              <MakeForm />
            </Route>
            <Route path="/my-forms">
              <MyForms />
            </Route>
            <Route path="/signin">
              <SignIn />
            </Route>
            <Route path="/signup">
              <SignUp />
            </Route>
            <Route path="/author-form">
              <AuthorForm />
            </Route>
            <Route path="/" exact>
              <Home />
            </Route>
            <Route path="*">
              <FillForm />
            </Route>
          </Switch>
        </div>
      </Router>
    </AuthProvider>
  );
}

export default App;
