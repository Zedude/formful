import firebase from 'firebase'
import { useState, useEffect } from 'react';
import 'firebase/auth'

const config = {
    apiKey: "AIzaSyDHHVPWZmpqHlIEMGlY37mD-j0UrpOChTA",
    authDomain: "form-ulaic.firebaseapp.com",
    projectId: "form-ulaic",
    storageBucket: "form-ulaic.appspot.com",
    messagingSenderId: "305979379967",
    appId: "1:305979379967:web:4e697f71e75780cd81bcca",
    measurementId: "G-6EHMCFM20K"
};

export const useFirebase = () => {
    const [state, setState] = useState({ firebase })

    useEffect(() => {
        let app;
        if (!firebase.apps.length) {
            app = firebase.initializeApp(config);
        }
        let auth = firebase.auth(app)
        let firestore = firebase.firestore(app);
        setState({ firebase, app, auth, firestore });
    }, [config]);

    return state;
}