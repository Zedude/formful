import React from 'react'

export const Answer = ({ question, children }) => {
   return (
      <div className='answer'>
         <div>{question}</div>
         {children}
      </div>
   )
}