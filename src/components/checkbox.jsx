import React, { createContext, useContext, useEffect, useState } from 'react'
import _ from 'lodash'

const CheckBoxContext = createContext({
   check: null,
   setCheck: () => { },
   choices: [],
   setChoices: () => { }
});

export const Checkbox = ({ options, multiple, setChoice }) => {
   const [check, setCheck] = useState(null)
   const [choices, setChoices] = useState([])

   useEffect(() => {
      if (check && setChoice) {
         setChoice(check)
      } else if (choices.length) {
         setChoice(choices)
      }
   })

   const render = () => {
      return options.map((e, i) => {
         return (
            <>
               {multiple ?
                  (<CheckboxItemMultiple option={e} index={i} />)
                  :
                  (<CheckboxItem option={e} index={i}></CheckboxItem>)
               }
            </>
         )
      })
   }

   return (
      <CheckBoxContext.Provider value={{ check, setCheck, multiple, setChoices, choices }}>
         <div className='checkboxContainer'>
            <div>
               {render()}
            </div>
         </div>
      </CheckBoxContext.Provider>
   )
}

const CheckboxItem = ({ option, index }) => {
   const { setCheck, check } = useContext(CheckBoxContext);
   const [checked, setChecked] = useState(false);

   useEffect(() => {
      check === index ? setChecked(true) : setChecked(false);
   }, [check]);

   const onClick = () => {
      if (check === index) {
         setCheck(null);
      } else {
         setCheck(index);
      }
   };

   return (
      <div className='checkboxItemContainer' onClick={onClick}>
         <div className={checked ? 'checkedbox' : 'checkbox'}></div>
         <div>{option}</div>
      </div>
   )
}

const CheckboxItemMultiple = ({ option, index }) => {
   const { setChoices, choices } = useContext(CheckBoxContext);
   const [choice, setChoice] = useState(false);

   useEffect(() => {
      _.includes(choices, index) ? setChoice(true) : setChoice(false);
   }, [choices]);

   const onClick = () => {
      if (_.includes(choices, index)) {
         setChoices(_.without(choices, index))
      } else {
         setChoices(_.concat(choices, index))
      }
   };

   return (
      <div className='checkboxItemContainer' onClick={onClick}>
         <div className={choice ? 'checkedbox' : 'checkbox'}></div>
         <div>{option}</div>
      </div>
   )
}