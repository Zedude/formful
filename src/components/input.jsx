import React from 'react'

export const Input = ({placeholder, width, type, ...other}) => {
   return (
      <input type={type} placeholder={placeholder} style={{width: width ? width : 'auto'}} className='inputContainer' {...other} />
   )
}