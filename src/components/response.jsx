import React from 'react'

export const Response = ({ response, question }) => {
    return (
        <div className='response'>
            <p>
                Question: {question}
            </p>
            <p>
                Response: {response}
            </p>
        </div>
    )
}