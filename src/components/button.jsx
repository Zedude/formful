import React from 'react'

export const Button = ({type, children, onClick, ...other}) => {
     return (
          <div className={`buttonContainer button${type ? type : 'primary'}`} onClick={() => {onClick && onClick()}} {...other}>
               {children}
          </div>
     )
}