import React, { useContext, useEffect, useState } from 'react'
import { useFirebase } from './firebase'

const AuthContext = React.createContext({
  user: null,
  signin: () => { },
  signup: () => { },
});

export const useAuth = () => {
  return useContext(AuthContext);
}

export const AuthProvider = ({ children }) => {
  const { auth } = useFirebase();
  const [user, setUser] = useState();

  const signup = (email, password) => {
    return auth.createUserWithEmailAndPassword(email, password)
  }

  const signin = (email, password) => {
    return auth.signInWithEmailAndPassword(email, password)
  }

  useEffect(() => {
    if (auth) {
      auth.onAuthStateChanged(user => {
        setUser(user)
      })
    }
  }, [auth])

  return (
    <AuthContext.Provider value={{user, signin, signup}}>
      {children}
    </AuthContext.Provider>
  )
}